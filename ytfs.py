#!/usr/bin/env python

import loopback, os, fuse, tempfile, sys, errno, shutil

class Ytfs(loopback.Loopback):
    def __init__(self):
        self.target = tempfile.mkdtemp()
        loopback.Loopback.__init__(self, self.target)

    def __del__(self):
        shutil.rmtree(self.target)

    def cannotWrite(self): raise fuse.FuseOSError(errno.EACCES)

    def chmod(self, path, mode): return self.cannotWrite()
    def chown(self, path, uid, gid): return self.cannotWrite()
    def create(self, path, mode): return self.cannotWrite()
    def link(self, target, source): return self.cannotWrite()
    def mknod(self, filename, mode=0600, device=0): return self.cannotWrite()
    def rename(self, old, new): return self.cannotWrite()
    def rmdir(self, path): return self.cannotWrite()
    def symlink(self, target, source): return self.cannotWrite()
    def truncate(self, path, length, fh=None): return self.cannotWrite()
    def unlink(self, path): return self.cannotWrite()
    def utimens(self, path, times): return self.cannotWrite()
    def write(self, path, data, offset, fh): return self.cannotWrite()

    def mkdir(self, path, mode):
        print "mkdir " + path
        return os.mkdir(path, mode)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: %s <mountpoint>' % sys.argv[0])
        sys.exit(1)

    fs = fuse.FUSE(Ytfs(), sys.argv[1], foreground=True)
